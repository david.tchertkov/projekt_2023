Als erstes sollte man eine Linux Instance erstellen.
![Alt-Text](./Medien/1%20Instance%20erstellen.png)

Nachdem man sich mit die Istanz verbindet, sollte man sich als Root User anmelden.
![Alt-Text](./Medien/2%20Als%20root%20user%20sein.png)

Nachdem man ein Root user ist, sollte man httpd installieren, ich habe hier apache 2 installiert weil ich die VM falsch installiert habe. Aber in den nächsten schritten ist es httpd.

![Alt-Text](./Medien/3%20installing%20apache%20server.png)

Als ich fertig war habe ich den status von den Service geprüft.

![Alt-Text](./Medien/4%20checking%20status%20of%20httpd.png)

Ich habe ein Directory folder erstellt damit ich mein CSS Template übersichtlich runterladen kann.

![Alt-Text](./Medien/5%20creating%20temp%20folder.png)

Nachdem ich den Folder erstellt habe, habe ich ein CSS Template ausgewählt.

![Alt-Text](./Medien/6%20css%20temaplate.png)

Dann habe ich es runtergeladen.

![Alt-Text](./Medien/7%20download%20css%20template.png)

Nachdem man es runtergeladen hat muss man es entpacken.

![Alt-Text](./Medien/8%20unzip%20file.png)

Später habe ich alles im "HTTP" Folder übertragen

![Alt-Text](./Medien/10%20move%20everything%20to%20http%20folder.png)

Als ich mich mit der öffentliche IP Adresse verbinden wollte, ging es nicht also habe ich bei der Firewall HTTP und HTTPS freigeschaltet

![Alt-Text](./Medien/9%20edit%20firewall.png)

Als es immer noch nicht funktionierte habe ich festgestellt das ich den Service noch starten muss.


![Alt-Text](./Medien/11%20starts%20httpd%20service.png)

Am ende hat es geklappt.

![Alt-Text](./Medien/12%20ende.png)








